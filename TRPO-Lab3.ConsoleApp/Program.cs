﻿using System;
using TRPO_Lab3.Lib;


namespace TRPO_Lab3.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите значение длины радиуса");
            string StringR = Console.ReadLine();
            double R = Convert.ToDouble(StringR);

            Console.WriteLine("Введите значение длины дуги");
            string Stringl = Console.ReadLine();
            double l = Convert.ToDouble(Stringl);

            var S = Formula.AreaSectorCircle(R, l);
            Console.WriteLine($"Площадь сектора круга равна {S}");
        }
    }
}
