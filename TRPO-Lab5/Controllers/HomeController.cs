﻿using Microsoft.AspNetCore.Mvc;
using System;
using TRPO_Lab3.Lib;


namespace TRPO_Lab5.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(string R, string l)
        {
            double S = Formula.AreaSectorCircle(Convert.ToDouble(R), Convert.ToDouble(l));
            ViewBag.result = S;
            return View();
        }


    }
}
